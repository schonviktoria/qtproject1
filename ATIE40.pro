#-------------------------------------------------
#
# Project created by QtCreator 2017-03-15T12:18:10
#
#-------------------------------------------------

QT       += core gui
QT       += testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ATIE40
TEMPLATE = app


SOURCES += main.cpp\
        view.cpp \
    model.cpp \
    modeltest.cpp

HEADERS  += view.h \
    model.h \
    modeltest.h
