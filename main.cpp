#include "view.h"
#include <QApplication>
#include "modeltest.h"

int main(int argc, char *argv[])
{
    ModelTest mtest;
    QTest::qExec(&mtest, argc, argv);

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();

}
