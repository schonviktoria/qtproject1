#include "model.h"

Model::Model()
{

}

void Model::newGame(int n)
{
    tableSize = n;
    middleOfTable = n/2 + 1;

    head = QPair<int, int>(middleOfTable, middleOfTable);
    for (int i = middleOfTable; i > middleOfTable-5; --i)
    {
        snake.push_back(QPair<int, int>(middleOfTable, i));
    }

    connect(&timerStep, SIGNAL(timeout()), this, SLOT(timerStepElapsed()));

    generateEggs();

    currentDir = RIGHT;
    eggCount = 0;
    snakeSize = 5;
    timerStep.setInterval(500);
    timerStep.start();
}

void Model::creep()
{
    if (currentDir == RIGHT)
    {
        head.second++;
    }

    if (currentDir == LEFT)
    {
        head.second--;
    }

    if (currentDir == UP)
    {
        head.first--;
    }

    if (currentDir == DOWN)
    {
        head.first++;
    }
}

void Model::timerStepElapsed()
{
    creep();

    snake.push_front(head);

    if (head == eggPos)
    {
        eggCount++;
        generateEggs();
    }
    else
    {
        snake.pop_back();
    }

    if (isEnd())
    {
        modelEndGame(eggCount);
        timerStep.stop();
    }
    else
    {
        modelTableChanged(snake, eggPos);
    }
}

void Model::generateEggs()
{
    do
    {
        eggPos.first  = qrand() % (tableSize - 1);
        eggPos.second = qrand() % (tableSize - 1);
    }

    while (snake.contains(eggPos));

}

bool Model::isEnd()
{
    return (head.first > tableSize-1 || head.second > tableSize-1
        || head.first < 0  || head.second < 0 || snake.count(head) > 1 );

}

void Model::changeDirection(directions d)
{
    if (timerStep.isActive())
    {
        if (!((d == RIGHT && currentDir == LEFT) || (d == LEFT && currentDir == RIGHT) ||
            (d == UP && currentDir == DOWN) || (d == DOWN && currentDir == UP)))
        currentDir = d;
    }

}

void Model::pause()
{
    if (timerStep.isActive())
    {
        timerStep.stop();
    }
    else
    {
        timerStep.start();
    }
}

int Model::getEggCount()
{
    return eggCount;
}

QLinkedList<QPair<int, int> > Model::getSnake()
{
    return snake;
}

QPair<int, int> Model::getHead()
{
    return head;
}

QPair<int, int> Model::getEggPos()
{
    return eggPos;
}
