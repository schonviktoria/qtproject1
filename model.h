#ifndef MODEL_H
#define MODEL_H

#include <QVector>
#include <QTimer>
#include <QPair>
#include <QLinkedList>
#include <QObject>

class Model: public QObject
{
     Q_OBJECT

public:
    Model();
    void newGame(int n);
    ~Model(){}
    enum directions
    {
        UP, RIGHT, DOWN, LEFT
    };

    void changeDirection(directions d);
    void pause();

    int getEggCount();
    QLinkedList<QPair<int, int> > getSnake();
    QPair<int, int> getHead();
    QPair<int, int> getEggPos();

private:
    QLinkedList<QPair<int, int> > snake;
    QPair<int, int> head;

    QTimer timerStep;

    int tableSize;
    int middleOfTable;

    directions currentDir;

    int snakeSize;

    QPair<int, int> eggPos;    
    int eggCount;
    void generateEggs();

    void creep();

    bool isEnd();

signals:
    void modelTableChanged(QLinkedList<QPair<int, int> > s, QPair<int, int> e);
    void modelEndGame(int e);

public slots:
    void timerStepElapsed();

};

#endif // MODEL_H
