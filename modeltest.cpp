#include "modeltest.h"

void ModelTest::initTestCase()
{
    model = new Model();
}

void ModelTest::cleanupTestCase()
{
    delete model;
}

void ModelTest::testNewGame()
{
    model->newGame(11);

    // eggCount, kígyó hossza stimmel -e, a tojást a kígyón kívülre generáltuk -e

    QCOMPARE(model->getEggCount(), 0);

    QCOMPARE(model->getSnake().size(), 5);

    QCOMPARE(model->getSnake().contains(model->getEggPos()), false);
}

void ModelTest::testStep()
{
    // egy lépés után a várt helyen van -e a kígyó "feje"

    model->newGame(11);
    QPair<int, int> headTmp = model->getHead();
    headTmp.second++;
    model->timerStepElapsed();
    QCOMPARE(model->getHead(), headTmp);
}

void ModelTest::testEndGame()
{
    // ilyen paraméterek mellett, 4 lépés után a kígyónak falba kell ütköznie

    model->newGame(11);
    QPair<int, int> headTmp = model->getHead();

    for (int i = 0; i < 4; ++i)
    {
        model->timerStepElapsed();
        headTmp.second++;
    }

    QCOMPARE(model->getHead(), headTmp);
}


