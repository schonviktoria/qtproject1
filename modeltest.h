#ifndef MODELTEST_H
#define MODELTEST_H

#include <QString>
#include <QtTest>
#include "model.h"

class ModelTest : public QObject
{
    Q_OBJECT

private:
    Model* model;
private slots:
    void initTestCase();
    void cleanupTestCase();
    void testNewGame();
    void testStep();
    void testEndGame();
};


//QTEST_APPLESS_MAIN(ModelTest)

//#include "modeltest.moc"

#endif // MODELTEST_H
