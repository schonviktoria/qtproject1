#include "view.h"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
	setWindowTitle("Snake game");
	
	button11 = new QPushButton( "11x11", this );
    button13 = new QPushButton( "13x13", this );
    button17 = new QPushButton( "17x17", this );
    buttonPause = new QPushButton( "Pause", this );

    menuLayout = new QHBoxLayout;
    menuLayout->addWidget( button11 );
    menuLayout->addWidget( button13 );
    menuLayout->addWidget( button17 );
    menuLayout->addWidget( buttonPause );

    fieldsLayout = new QGridLayout;
    fieldsLayout->setSpacing(0);

    mainLayout = new QVBoxLayout( this );
    mainLayout->addLayout( menuLayout );
    mainLayout->addLayout( fieldsLayout );

    connect(button11, SIGNAL(clicked(bool)), this, SLOT(newGame11()) );
    connect(button13, SIGNAL(clicked(bool)), this, SLOT(newGame13()) );
    connect(button17, SIGNAL(clicked(bool)), this, SLOT(newGame17()) );
    connect(buttonPause, SIGNAL(clicked(bool)), this, SLOT(buttonPauseClick()) );

    model = 0;
}

void MainWindow::newGameTmp(int n)
{
    delete model;
    tableSize = n;

    for (int i = 0; i < fields.size(); ++i)
    {
        for (int j = 0; j < fields[i].size(); ++j)
        {
            fieldsLayout->removeWidget(fields[i][j]);
            delete fields[i][j];
        }
    }

    setFixedSize(n*25, n*25+40);

    fields.resize(n);

    for (int i = 0; i < n; ++i)
    {
        fields[i].resize(n);
        for (int j = 0; j < n; ++j)
        {
            fields[i][j] = new QPushButton(this);
            fields[i][j]->setFixedSize(20, 20);
            fieldsLayout->addWidget(fields[i][j], i, j);
        }
    }

    model = new Model();

    connect(model, SIGNAL(modelTableChanged(QLinkedList<QPair<int, int> >, QPair<int, int>)), this,
            SLOT(tableChanged(QLinkedList<QPair<int, int> >, QPair<int, int>)));

    connect(model, SIGNAL(modelEndGame(int)), this, SLOT(endGame(int)));

}

void MainWindow::tableChanged(QLinkedList<QPair<int, int> > s, QPair<int, int> e)
{
    for (int i = 0; i < tableSize; ++i)
    {
        for (int j = 0; j < tableSize; ++j)
        {
            fields[i][j]->setStyleSheet("background-color: none");
        }
    }

    typedef QLinkedList<QPair<int, int> >::const_iterator iter;
    for (iter i = s.begin(); i != s.end(); ++i)
    {
        fields[i->first][i->second]->setStyleSheet("background-color: black");
    }
    fields[e.first][e.second]->setStyleSheet("background-color: yellow");
}

void MainWindow::endGame(int e)
{
    QMessageBox m(this);
	m.setText(QString::fromUtf8("Vége a játéknak! Pontszám: ") + QString::number(e));
    m.exec();
}

void MainWindow::newGame11()
{
    newGameTmp(11);
    model->newGame(11);
}

void MainWindow::newGame13()
{
    newGameTmp(13);
    model->newGame(13);
}

void MainWindow::newGame17()
{
    newGameTmp(17);
    model->newGame(17);
}

void MainWindow::keyPressEvent(QKeyEvent *keyevent)
{
	switch (keyevent->key())
	{
		case Qt::Key_A:
		{
			model->changeDirection(Model::LEFT);
			break;
		}
		case Qt::Key_D:
		{
			model->changeDirection(Model::RIGHT);
			break;
		}
		case Qt::Key_S:
		{
			model->changeDirection(Model::DOWN);
			break;
		}
		case Qt::Key_W:
		{
			model->changeDirection(Model::UP);
			break;
		}
	}
}


void MainWindow::buttonPauseClick()
{
    model->pause();
}

MainWindow::~MainWindow()
{

}
