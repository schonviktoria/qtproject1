#ifndef VIEW_H
#define VIEW_H

#include <QWidget>
#include <QVector>
#include <QPushButton>
#include <QLayout>
#include <QMessageBox>
#include <QKeyEvent>
#include "model.h"

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:

    QPushButton *button11;
    QPushButton *button13;
    QPushButton *button17;
    QPushButton *buttonPause;

    QVBoxLayout *mainLayout;
    QHBoxLayout *menuLayout;
    QGridLayout *fieldsLayout;

    QVector<QVector<QPushButton*> > fields;

    Model *model;

    int tableSize;

protected:
	void keyPressEvent(QKeyEvent *keyevent);

private slots:
    void newGameTmp(int n);
    void newGame11();
    void newGame13();
    void newGame17();
    void buttonPauseClick();
    void tableChanged(QLinkedList<QPair<int, int> > s, QPair<int, int> e);
    void endGame(int e);
};

#endif // VIEW_H
